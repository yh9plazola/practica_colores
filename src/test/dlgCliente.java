package test;

import java.net.*;
import java.io.*;

public class dlgCliente extends javax.swing.JDialog {
    private PrintStream salida = null;
    private Socket ClienteSocket = null; // Socket
    private String IP="127.0.0.1"; //Ip, domino, o Nombre de la maquina
    private final int PUERTO = 5555;
    private String figuraSeleccionada = "";
    private String colorSeleccionado = "";
    public dlgCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        try {
            ClienteSocket = new Socket(IP, PUERTO);
            salida = new PrintStream(ClienteSocket.getOutputStream());
            salida.println("\t");
            salida.println("iniciando");
        } catch (UnknownHostException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
            // Puedes agregar un mensaje para el usuario aquí si lo deseas
        } catch (IOException e) {
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace();
            // Puedes agregar un mensaje para el usuario aquí si lo deseas
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgFiguras = new javax.swing.ButtonGroup();
        bgColores = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        btnEnviar = new javax.swing.JButton();
        rdbRojo = new javax.swing.JRadioButton();
        rdbCirculo = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        rdbRectangulo = new javax.swing.JRadioButton();
        rdbAzul = new javax.swing.JRadioButton();
        rdbVerde = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cliente", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 0, 18))); // NOI18N
        jPanel1.setLayout(null);

        btnEnviar.setText("Enviar Tarea");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });
        jPanel1.add(btnEnviar);
        btnEnviar.setBounds(10, 200, 210, 28);

        bgColores.add(rdbRojo);
        rdbRojo.setText("Rojo");
        jPanel1.add(rdbRojo);
        rdbRojo.setBounds(20, 150, 150, 21);

        bgFiguras.add(rdbCirculo);
        rdbCirculo.setSelected(true);
        rdbCirculo.setText("Circulo");
        jPanel1.add(rdbCirculo);
        rdbCirculo.setBounds(20, 50, 130, 21);

        jLabel1.setText("---------------------------");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 30, 150, 16);

        jLabel2.setText("---------------------------");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 90, 150, 16);

        bgFiguras.add(rdbRectangulo);
        rdbRectangulo.setText("Rectangulo");
        jPanel1.add(rdbRectangulo);
        rdbRectangulo.setBounds(20, 70, 140, 21);

        bgColores.add(rdbAzul);
        rdbAzul.setSelected(true);
        rdbAzul.setText("Azul");
        jPanel1.add(rdbAzul);
        rdbAzul.setBounds(20, 110, 140, 21);

        bgColores.add(rdbVerde);
        rdbVerde.setText("Verde");
        jPanel1.add(rdbVerde);
        rdbVerde.setBounds(20, 130, 140, 21);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 230, 240);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        // TODO add your handling code here:
        if (rdbCirculo.isSelected()) {
            figuraSeleccionada = "Circulo";
        } else if (rdbRectangulo.isSelected()) {
            figuraSeleccionada = "Rectangulo";
        }

        if (rdbAzul.isSelected()) {
            colorSeleccionado = "Azul";
        } else if (rdbVerde.isSelected()) {
            colorSeleccionado = "Verde";
        } else if (rdbRojo.isSelected()) {
            colorSeleccionado = "Rojo";
        }

        // Enviar tarea al servidor
        String tarea = figuraSeleccionada + "," + colorSeleccionado;
        salida.println(tarea);
    }//GEN-LAST:event_btnEnviarActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgCliente dialog = new dlgCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setSize(266, 300);
                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgColores;
    private javax.swing.ButtonGroup bgFiguras;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton rdbAzul;
    private javax.swing.JRadioButton rdbCirculo;
    private javax.swing.JRadioButton rdbRectangulo;
    private javax.swing.JRadioButton rdbRojo;
    private javax.swing.JRadioButton rdbVerde;
    // End of variables declaration//GEN-END:variables
}
